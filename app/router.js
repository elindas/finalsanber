import React from "react";

import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";

//IMPORT SCENES
import DashBoardScreen from "./scenes/dashboard/DashBoard";
import ArticleScreen from "./scenes/dashboard/Article";
import LoginScreen from "./scenes/LoginScreen";
import AboutScreen from "./scenes/AboutScreen";

//==========================================================

let font = "Roboto";
let titleColor = "#363434";

//Nav Header Styles
let headerTitleStyle = {
  fontWeight: "bold",
  fontSize: 17,
  fontFamily: font,
  color: titleColor,
};

//ROUTER ====================================================
const Tabs = createBottomTabNavigator();
const Stack = createStackNavigator();

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen
      name="Dashboard"
      component={DashBoardScreen}
      options={{
        title: "Home",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="home" color={color} size={25} />
        ),
      }}
    />
    <Tabs.Screen
      name="About"
      component={AboutScreen}
      options={{
        title: "About",
        tabBarIcon: ({ color, size }) => (
          <MaterialCommunityIcons name="account" color={color} size={size} />
        ),
      }}
    />
  </Tabs.Navigator>
);
export default function DashboardStack(props) {
  const { route } = props;
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        headerMode="screen"
        screenOptions={{
          headerStyle: { backgroundColor: "#fff" },
          headerTitleStyle: headerTitleStyle,
        }}
      >
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{ headerShown: false }}
        />
        {/* <Stack.Screen
          name="Dashboard"
          component={DashBoardScreen}
          options={{ title: "News" }}
        /> */}
        <Stack.Screen
          name="About"
          component={AboutScreen}
          options={{ headerShown: true }}
        />
        <Stack.Screen
          name="Article"
          component={ArticleScreen}
          options={({ route }) => ({ title: route.params.title })}
        />
        <Stack.Screen
          name="TabScreen"
          component={TabsScreen}
          options={{ title: "News" }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
