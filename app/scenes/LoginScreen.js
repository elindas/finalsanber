import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
} from "react-native";

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }
  loginHandler() {
    this.props.navigation.navigate("TabScreen", {
      email: this.state.email,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require("../assets/logo.png")}
          style={{ width: 350, height: 100 }}
        />
        <View style={{ marginTop: 45 }}>
          <Text style={{ color: "#003366", fontSize: 28 }}>Login</Text>
        </View>

        <View style={styles.formContainer}>
          <Text style={styles.label}>Email</Text>
          <TextInput
            style={styles.inputForm}
            onChangeText={(email) => this.setState({ email })}
          ></TextInput>
          <Text style={styles.label}>Password</Text>
          <TextInput
            style={styles.inputForm}
            onChangeText={(password) => this.setState({ password })}
            secureTextEntry={true}
          ></TextInput>
        </View>

        <TouchableOpacity
          style={styles.ButtonContainer}
          onPress={() => this.loginHandler()}
        >
          <Text style={styles.ButtonText}>Login</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    justifyContent: "center",
    alignItems: "center",
  },

  formContainer: {
    paddingTop: 30,
  },
  label: {
    color: "#003366",
    fontSize: 14,
    padding: 5,
  },
  inputForm: {
    height: 48,
    width: 294,
    borderColor: "#003366",
    borderWidth: 1,
    backgroundColor: "#FFFFFF",
  },

  LoginText: {
    marginTop: 50,
    fontSize: 24,
    alignSelf: "center",
    fontWeight: "bold",
  },

  ButtonContainer: {
    marginTop: 46,
    width: 294,
    height: 60,
    backgroundColor: "#3498DB",
    alignSelf: "center",
    justifyContent: "center",
    borderRadius: 3,
  },
  ButtonText: {
    fontSize: 16,
    alignSelf: "center",
    color: "#FFFFFF",
  },
});
